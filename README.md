# Simple GitLab pipeline

**Tier:** 1-Beginner

You are responsible for configuring a simple GitLab pipeline to learn the basic of CI/CD.

## User Stories

-   [ ] Create a public GitLab project
-   [ ] Create the .gitlab-ci.yml with the following stages: build, test and deploy using the latest alpine docker image to run the following jobs
-   [ ] build:build-job1 echo "This is the Job1"
-   [ ] test:test-job1 echo "This is the test" and it waits 5 seconds
-   [ ] deploy:deploy-job1 echo "This is the deploy" and it waits 10 seconds

## Bonus features

-   [ ] After the deploy stage is done, echo the email of the user who started the job

## Useful links and resources

- [GitLab CI/CD Getting Started](https://docs.gitlab.com/ee/ci/quick_start/)

## Example projects

- [GitLab CI/CD with docker](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Bash.gitlab-ci.yml)

# Intermediate GitLab pipeline

**Tier:** 2-Intermediate

You are responsible for configuring a GitLab pipeline that will use terraform code to allow an EC2 instance to communicate with a Dynamo table

## User Stories

-   [ ] Create the .gitlab-ci.yml with the following stages: validate (lint), format, plan and deploy. The last stage needs
-   [ ] The plan stage needs to print the differences of the current and expected infrastructure
-   [ ] Create two GitLab environments: Staging and Production
-   [ ] Configure the deploy stage to wait for a manual approval for the Production environment. The approver will review the terraform plan and proceed with the deployment

## Bonus features

-   [ ] Store the terraform state remotely (S3 bucket)
-   [ ] Configure the production environment to be slightly different than the staging environment

## Useful links and resources

- [Terraform aws_dynamodb_table resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table)
- [Terraform aws_instance resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)
- [Terraform instance profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile)
- [Introduction to Terraform](https://www.terraform.io/intro/index.html)

## Example projects

- Null

# Advanced GitLab pipeline

**Tier:** 3-Advanced

You are responsible for configuring a GitLab pipeline that will use terraform code to do the staging(disposable) environment DynamoDB table provisioning, build, deploy and test a JAVA application that follows the 12-Factors wrapped as docker image. You should use ECS Fargate to host the application. Jacoco will be used to assess the application. In either case, if the application test runs with failure, destroy the environment and notify the developers. In case it runs with success, create the production environment (if it does not exist), deploy the application, test and notify the developer.

## User Stories

-   [ ] Create the .gitlab-ci.yml with the following stages: validate (lint), format, plan and deploy the infrastructure
-   [ ] Create the .gitlab-ci.yml with the following stages: test, code-quality, build, publish, staging, scan, release, production
-   [ ] The plan stage needs to print the differences of the current and expected infrastructure
-   [ ] Create one GitLab environment: Staging
-   [ ] Configure the deploy stage to wait for a manual approval for the Production environment. The approver will review the terraform plan and application test results and proceed with the deployment

## Bonus features

-   [ ] Store the terraform state in GitLab

## Useful links and resources

- [Terraform aws_dynamodb_table resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table)
- [Terraform aws_instance resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)
- [Terraform instance profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile)
- [Introduction to Terraform](https://www.terraform.io/intro/index.html)

## Example projects

- Null